import { defineConfig , loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'

// 如果编辑器提示 path 模块找不到，则可以安装一下 @types/node -> npm i @types/node -D
import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig(({ mode, command }) => ({
  build: { //打包去掉console信息
    minify: "terser",
    terserOptions: {
      compress: {
        drop_console: command === "build" && loadEnv(mode, __dirname).VITE_API_ENV === "prod",
      }
    }
  },
  plugins: [vue()],
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src') // 设置 `@` 指向 `src` 目录
    }
  },
  base: './', // 设置打包路径
  server: {
    port: 4001, // 设置服务启动端口号
    open: true, // 设置服务启动时是否自动打开浏览器
    cors: true, // 允许跨域
    host:'0.0.0.0',
    // // 设置代理，根据我们项目实际情况配置
    proxy: {
      // '/ai-fast-training': {
      //   target: 'http://117.185.57.6:15557',
      //   changeOrigin: true,
      // secure: false,
      //   rewrite: (path) => path.replace(/^\/snow/, '')
      // }
    }
  }
}))

